# Simple Profile Page

This project is to create a simple responsive profile page. Then, the page will be deployed to a free web hosting "Firebase".


  - [Description](#description)
  - [Resources](#resources)
  - [Installation](#installation)
    - [Source Code Preparation](#source-code-preparation)
    - [Firebase Setup](#firebase-setup)

## Description

Sometimes, you may want to let people know more about you and exchange your contact especially during making new friends. In the past, people liked to give their name card or business card to new friends.  Currently, we can provide QR code which is converted to a web page link so that your friends can view the page with all the information which you want to present including facebook, Whatsapp, telegram or email address.  In addition, Firebase provides a free web hosting so we can make use of it. Of course, you may customise the page or even create multiple pages in your website. This project is just providing a starting point for you.

## Resources
Bootstrap is applied to build a responsive web page.
[Link](https://getbootstrap.com/)

Fontawsome is used to create amazing icons.  [Link](https://fontawesome.com/)

Firebase provides a free hosting. [Link](https://firebase.google.com/)

QR code generator is used to convert your web page URL to a QR code. [Link](https://www.the-qrcode-generator.com/)


## Installation
### Source code preparation
1. Download the source code
2. Replace those dummy hyper links with your own links for facebook, whatsapp, telegram, email and gitlab.
3. Customise the index.html if you want.

### Firebase Setup
1. You may follow the instruction on youtube to create your project for hosting the web page.
[https://www.youtube.com/watch?v=w7xKZ5PWizs](https://www.youtube.com/watch?v=w7xKZ5PWizs)

  This video is not made by me. Credit is to Sameer Saini. Thanks for the detailed tutorial.

2. After watching the tutorial, you may know how to create and deploy a project to firebase. All you need to deploy is the assets folder with all the files inside it and the index.html.
